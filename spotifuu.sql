-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2024 at 01:25 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spotifuu`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(10) NOT NULL,
  `album_name` varchar(50) NOT NULL,
  `album_image` text DEFAULT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_name`, `album_image`, `user_id`) VALUES
(1, 'album 01', NULL, 3),
(2, 'album 02', NULL, 4),
(3, 'album 03', NULL, 3),
(4, 'album 04', NULL, 7),
(5, 'album 05', NULL, 9),
(6, 'album 06', NULL, 10),
(7, 'album 07', NULL, 1),
(8, 'album 08', NULL, 7),
(9, 'album 09', NULL, 10),
(10, 'album 10', NULL, 7),
(11, 'album 11', NULL, 3),
(12, 'NJWMX', NULL, 5),
(13, 'Cosmic', NULL, 9),
(14, 'We love you tecca', NULL, 10),
(15, 'ลูกทุ่งกีตาร์หวาน ไมค์ ภิรมย์พร 2', NULL, 5),
(16, 'Heartbreaker (I Broke Your Heart)', NULL, 1),
(17, 'Mising only you', NULL, 1),
(18, 'For tonight', NULL, 7),
(19, 'ไม่ไหวบอกไหว', NULL, 16),
(20, 'Phossil', NULL, 12);

-- --------------------------------------------------------

--
-- Table structure for table `album_detail`
--

CREATE TABLE `album_detail` (
  `albumdetail_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `music_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `album_detail`
--

INSERT INTO `album_detail` (`albumdetail_id`, `album_id`, `music_id`) VALUES
(1, 20, 8),
(2, 19, 21),
(3, 12, 17),
(4, 12, 20),
(5, 13, 12),
(6, 13, 13),
(7, 13, 14),
(8, 13, 15),
(9, 14, 1),
(10, 14, 2),
(11, 14, 3),
(12, 14, 4),
(13, 15, 16),
(14, 16, 9),
(15, 17, 10),
(16, 18, 11);

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `bill_id` int(11) NOT NULL,
  `bill_cost` int(11) NOT NULL,
  `categories` int(11) NOT NULL,
  `bill_create` datetime NOT NULL,
  `bill_end` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bill_id`, `bill_cost`, `categories`, `bill_create`, `bill_end`, `user_id`) VALUES
(1, 10000, 1, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 1),
(2, 10000, 1, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 1),
(3, 10000, 2, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 2),
(4, 10000, 2, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 4),
(5, 10000, 2, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 3),
(6, 10000, 2, '2024-02-18 02:04:39', '2024-03-19 02:04:39', 9),
(7, 139, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 2),
(8, 359, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 5),
(9, 359, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 9),
(10, 359, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 10),
(11, 139, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 4),
(12, 139, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 1),
(13, 139, 1, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 8),
(14, 139, 2, '2024-02-19 15:13:15', '2024-03-20 15:13:15', 8);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL,
  `genre_name` varchar(50) NOT NULL,
  `genre_image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genre_id`, `genre_name`, `genre_image`) VALUES
(1, 'Rock', NULL),
(2, 'POP', NULL),
(3, 'Jazz', NULL),
(4, 'T-pop', NULL),
(5, 'HipHop', NULL),
(6, 'K-pop', NULL),
(7, 'R&B', NULL),
(8, 'Latin-pop', NULL),
(9, 'EDM', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genre_detail`
--

CREATE TABLE `genre_detail` (
  `genredetail_id` int(11) NOT NULL,
  `music_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `genre_detail`
--

INSERT INTO `genre_detail` (`genredetail_id`, `music_id`, `genre_id`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 2, 3),
(4, 3, 2),
(5, 4, 2),
(6, 5, 2),
(7, 6, 2),
(8, 6, 4),
(9, 7, 6),
(10, 7, 9),
(11, 7, 7),
(12, 9, 2),
(13, 10, 2),
(14, 11, 2),
(15, 21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `music_id` int(11) NOT NULL,
  `music_image` text DEFAULT NULL,
  `music_name` varchar(50) NOT NULL,
  `music_longtime` time DEFAULT NULL,
  `num_play` int(10) NOT NULL DEFAULT 0,
  `music_file` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`music_id`, `music_image`, `music_name`, `music_longtime`, `num_play`, `music_file`) VALUES
(1, NULL, 'รักแรก', '04:26:00', 1251, NULL),
(2, NULL, 'โต๊ะริม', '04:07:00', 1002, NULL),
(3, NULL, 'พิง', '04:02:00', 872, NULL),
(4, NULL, 'คิดถึงจัง', '03:50:00', 512, NULL),
(5, NULL, 'ยิ้ม', '03:39:00', 1103, NULL),
(6, NULL, 'ที่รัก', '03:46:00', 982, NULL),
(7, NULL, 'เมื่อวาน', '04:18:00', 1015, NULL),
(8, NULL, 'เธอนะมีของดี', '02:19:00', 2645, NULL),
(9, NULL, 'Heartbeaker (I broke your heart)', '02:10:00', 2051, NULL),
(10, NULL, 'Missing only you', '03:00:00', 1804, NULL),
(11, NULL, 'For tonight', '03:52:00', 1520, NULL),
(12, NULL, 'Super shine', '04:10:00', 1428, NULL),
(13, NULL, 'OMG', '03:58:00', 1412, NULL),
(14, NULL, 'พี่ๆ ตัดแว่นให้หน่อย', '03:50:00', 1359, NULL),
(15, NULL, 'Amigo', '03:39:00', 1261, NULL),
(16, NULL, 'น้ำตาลูกผู้ชาย', '03:57:00', 0, NULL),
(17, NULL, 'Ditto (250 Remix)', '03:21:00', 0, NULL),
(18, NULL, 'OMG (FENK Remix)', '03:30:00', 0, NULL),
(19, NULL, 'Attention (250 Remix)', '03:01:00', 0, NULL),
(20, NULL, 'Hype Boy (250 Remix)', '04:11:00', 0, NULL),
(21, NULL, 'ไม่ไหวบอกไหว', '04:06:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `playlist_id` int(11) NOT NULL,
  `playlist_name` varchar(50) NOT NULL,
  `playlist_image` text DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`playlist_id`, `playlist_name`, `playlist_image`, `user_id`) VALUES
(1, 'maditate', NULL, 15),
(2, 'i miss her', NULL, 15),
(3, 'ใจบางๆ', NULL, 15),
(4, 'hapi hapi', NULL, 15),
(5, 'chad', NULL, 15),
(6, 'soy bad', NULL, 15),
(7, 'i\'m sexy :P', NULL, 15);

-- --------------------------------------------------------

--
-- Table structure for table `playlist_detail`
--

CREATE TABLE `playlist_detail` (
  `playlistdetail_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `music_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `playlist_detail`
--

INSERT INTO `playlist_detail` (`playlistdetail_id`, `playlist_id`, `music_id`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(3, 3, 16),
(4, 2, NULL),
(5, 4, NULL),
(6, 2, NULL),
(7, 2, NULL),
(8, 4, NULL),
(9, 3, 11),
(10, 3, 9),
(11, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `upload_id` int(11) NOT NULL,
  `categories_upload` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `upload`
--

INSERT INTO `upload` (`upload_id`, `categories_upload`, `user_id`) VALUES
(1, 'single', 2),
(2, 'album', 1),
(3, 'album', 12),
(4, 'single', 4),
(5, 'single', 7),
(6, 'single', 7);

-- --------------------------------------------------------

--
-- Table structure for table `upload_detail`
--

CREATE TABLE `upload_detail` (
  `uploaddetail_id` int(11) NOT NULL,
  `upload_status` varchar(50) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `music_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `upload_detail`
--

INSERT INTO `upload_detail` (`uploaddetail_id`, `upload_status`, `upload_id`, `music_id`) VALUES
(1, 'Pending', 1, 1),
(2, 'Denieded', 4, 2),
(3, 'Pending', 6, 3),
(4, 'Pending', 2, 5),
(5, 'Pending', 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_image` text DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `display_name`, `password`, `user_image`, `email`, `role`) VALUES
(1, 'Phichet Parto', 'Chet', 'pass1234', NULL, 'email@gmail.com', 'Artist'),
(2, 'User01', 'user1', 'pass1234', NULL, 'email1@gmail.com', 'Listener'),
(3, 'User02', 'user2', 'pass1234', NULL, 'email2@gmail.com', 'Artist'),
(4, 'User03', 'user3', 'pass1234', NULL, 'email3@gmail.com', 'Artist'),
(5, 'User04', 'user4', 'pass1234', NULL, 'email4@gmail.com', 'Artist'),
(6, 'User05', 'user5', 'pass1234', NULL, 'email5@gmail.com', 'Listener'),
(7, 'User06', 'user6', 'pass1234', NULL, 'email6@gmail.com', 'Artist'),
(8, 'User07', 'user7', 'pass1234', NULL, 'email7@gmail.com', 'Listener'),
(9, 'User08', 'user8', 'pass1234', NULL, 'email8@gmail.com', 'Artist'),
(10, 'User09', 'user9', 'pass1234', NULL, 'email9@gmail.com', 'Artist'),
(11, 'User10', 'user10', 'pass1234', NULL, 'email10@gmail.com', 'Listener'),
(12, 'phossil', 'phossil', 'pass1234', NULL, 'email11@gmail.com', 'Artist'),
(13, 'mashmello', 'mashmello', 'pass1234', NULL, 'email12@gmail.com', 'Artist'),
(14, 'new jeans', 'new jeans', 'pass1234', NULL, 'email13@gmail.com', 'Artist'),
(15, 'lalalalaliza123', 'lalalalaliza123', 'pass1234', NULL, 'lalalalaliza123@gmail.com', 'Listener'),
(16, 'mike', 'ไมค์ ภิรมย์พร', 'pass1234', NULL, 'mike123@gmail.com', 'Artist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `album_detail`
--
ALTER TABLE `album_detail`
  ADD PRIMARY KEY (`albumdetail_id`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indexes for table `genre_detail`
--
ALTER TABLE `genre_detail`
  ADD PRIMARY KEY (`genredetail_id`);

--
-- Indexes for table `music`
--
ALTER TABLE `music`
  ADD PRIMARY KEY (`music_id`);

--
-- Indexes for table `playlist_detail`
--
ALTER TABLE `playlist_detail`
  ADD PRIMARY KEY (`playlistdetail_id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `upload_detail`
--
ALTER TABLE `upload_detail`
  ADD PRIMARY KEY (`uploaddetail_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `album_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
